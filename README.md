# Temple

Temple generates an html page based on sermon metadata scraped from a bulletin
pdf

## Directions

1. Save bulletin pdf to `input/bulletin.pdf`
1. Save audio link to `input/url.txt`
1. Run `./run.sh`
1. Profit

## Requirements

- Elixir
- Mix
- Poppler-utils (pdftotext)

## Example

Create a file called `copy-paste.txt` that looks like this:

```
Preaching of the Word Rev. Tyler Dirks

Power Perfected Through Weakness

Judges 16:23-31
I. Inclusion Of Undesirable Characters

II. Inescapable Weakness

III. Indomitable

- 4 -

- Reflection Questions -
1) How is perfection achieved? What’s the formula?
2) What’s your M.O. when it comes to dealing with undesirable characters?
3) How do you feel about weakness?
4) How do you feel about Paul’s fomenting use of the word ‘skubala’ in Philippians 3?
5) What is the most indomitable event in human history?
6) Who is the most indomitable figure in existence?
```

Then create a file that holds the dropbox audio url called `url.txt`:

```
https://www.dropbox.com/scl/fi/p1wz502r9bi189wsgnimj/ECPC-Worship-20231001.mp3?rlkey=mkpev3dtwhhw07l9dtfdt6ifa&dl=0
```

Then you can run `mix temple.run` and you'll see:

```html
<p style="text-align: center;"><strong>Sunday Worship</strong></p>
<p style="text-align: center;"><strong>October 1st, 2023</strong></p>
&nbsp;
<p style="text-align: center;"><strong>“Power Perfected Through Weakness”</strong></p>
<p style="text-align: center;">Judges 16:23-31</p>
<p style="text-align: center;">Rev. Tyler Dirks</p>
&nbsp;

<a href="https://dl.dropbox.com/scl/fi/p1wz502r9bi189wsgnimj/ECPC-Worship-20231001.mp3?rlkey=mkpev3dtwhhw07l9dtfdt6ifa&amp;dl=0
">Sermon Audio</a>

&nbsp;

<strong>Sermon Outline:</strong>
<ol>
  
 	<li>Inclusion Of Undesirable Characters</li>
  
 	<li>Inescapable Weakness</li>
  
 	<li>Indomitable</li>
  
</ol>
<strong>Reflection Questions:</strong>
<ol>
  
 	<li>How is perfection achieved? What’s the formula?</li>
  
 	<li>What’s your M.O. when it comes to dealing with undesirable characters?</li>
  
 	<li>How do you feel about weakness?</li>
  
 	<li>How do you feel about Paul’s fomenting use of the word ‘skubala’ in Philippians 3?</li>
  
 	<li>What is the most indomitable event in human history?</li>
  
 	<li>Who is the most indomitable figure in existence?</li>
  
</ol>
```
