defmodule TempleTest do
  use ExUnit.Case
  doctest Temple

  test "basic example" do
    copy_paste = ~S"""
    Preaching of the Word Rev. Tyler Dirks

    Christian Maturity
    Colossians 2:6-7

    I. As You Received Christ

    II. Rooted, Built Up, & Est. in the Faith

    III. Abounding In Thanksgiving

    - 4 -

    - Reflection Questions -

    1) How did you first receive Jesus? When you first received Jesus how prominent was your sense of desperation and neediness?
    2) What’s your picture of a “Mature Christian”? What was the prevailing assumption regarding “religious maturity” in Jesus’ day?
    3) How has Jesus continued to force you to feel vulnerable and desperately dependent (to keep you from becoming conceited, and to perfect His power in you)?
    4) How GRATEFUL are you on a weekly basis for the ROOT system of Jesus’ forgiveness and eccentric discipleship?
    """

    url = ~S"""
    https://www.dropbox.com/scl/fi/pfz7o8q4iz6d1iaqwvak6/ECPC-Worship-20240218.mp3?rlkey=ba8h76y32eotsuykcso7xl158&dl=0
    """

    final = ~s(
<p style="text-align: center;"><strong>Sunday Worship</strong></p>
<p style="text-align: center;"><strong>February 18th, 2024</strong></p>
&nbsp;
<p style="text-align: center;"><strong>“Christian Maturity”</strong></p>
<p style="text-align: center;">Colossians 2:6-7</p>
<p style="text-align: center;">Rev. Tyler Dirks</p>
&nbsp;

<a href="https://dl.dropbox.com/scl/fi/pfz7o8q4iz6d1iaqwvak6/ECPC-Worship-20240218.mp3?rlkey=ba8h76y32eotsuykcso7xl158&amp;dl=0
">Sermon Audio</a>

&nbsp;

<strong>Sermon Outline:</strong>
<ol>
  
 	<li>As You Received Christ</li>
  
 	<li>Rooted, Built Up, & Est. in the Faith</li>
  
 	<li>Abounding In Thanksgiving</li>
  
</ol>
<strong>Reflection Questions:</strong>
<ol>
  
 	<li>How did you first receive Jesus? When you first received Jesus how prominent was your sense of desperation and neediness?</li>
  
 	<li>What’s your picture of a “Mature Christian”? What was the prevailing assumption regarding “religious maturity” in Jesus’ day?</li>
  
 	<li>How has Jesus continued to force you to feel vulnerable and desperately dependent (to keep you from becoming conceited, and to perfect His power in you\)?</li>
  
 	<li>How GRATEFUL are you on a weekly basis for the ROOT system of Jesus’ forgiveness and eccentric discipleship?</li>
  
</ol>)

    assert Temple.main(copy_paste, url) |> String.trim() == final |> String.trim()
  end
end
