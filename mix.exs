defmodule Temple.MixProject do
  use Mix.Project

  def project do
    [
      app: :temple,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nimble_parsec, "~> 1.0"},
      {:ordinal, "~> 0.2.0"}
    ]
  end

  defp aliases do
    [
      run: ["temple.build", "temple.link", "temple.run"]
    ]
  end
end
