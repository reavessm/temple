#!/usr/bin/env bash

git add -A
git commit -sm "$(date +%Y-%m-%d)"
git push
