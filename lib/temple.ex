defmodule Temple do
  def main(sermon, url) do
    sermon
    |> Temple.Parser.parse()
    |> Temple.Model.build()
    |> Temple.Generator.generate(url)
  end
end
