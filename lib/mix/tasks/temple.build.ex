defmodule Mix.Tasks.Temple.Build do
  use Mix.Task

  def run(_) do
    {file_content, _code} = System.shell("pdftotext input/bulletin.pdf -f 4 -raw -")

    file_content = remove_before_pattern(file_content, "Preaching of the Word")

    [before, aft] = String.split(file_content, "-Reflection Questions -", parts: 2)

    joined_questions =
      aft
      |> String.split("\n")
      |> Enum.reduce([], &join_questions/2)
      |> Enum.reverse()

    # Combine the parts back
    file = before <> "-Reflection Questions -\n" <> Enum.join(joined_questions, "\n")

    sun = Date.utc_today() |> Date.beginning_of_week(:sunday) |> Date.to_string()
    File.write!("past/" <> sun <> "-copy-paste.txt", file)

    File.write!("past/" <> sun <> "-url.txt", File.read!("input/url.txt") |> String.trim())
  end

  defp remove_before_pattern(content, pattern) do
    [_before, aft] = String.split(content, pattern, parts: 2)
    pattern <> aft
  end

  defp join_questions(line, []), do: [line]

  defp join_questions(line, acc) do
    cond do
      String.match?(line, ~r/^\d+\./) -> [line | acc]
      true -> [List.first(acc) <> " " <> String.trim(line) | tl(acc)]
    end
  end
end
