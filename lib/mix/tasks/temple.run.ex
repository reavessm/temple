defmodule Mix.Tasks.Temple.Run do
  use Mix.Task

  def run(_) do
    Temple.main(File.read!("copy-paste.txt"), File.read!("url.txt"))
    |> IO.puts()
  end
end
