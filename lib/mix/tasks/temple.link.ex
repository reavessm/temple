defmodule Mix.Tasks.Temple.Link do
  use Mix.Task

  def run(_) do
    past_files = File.ls!("past")

    ["url.txt", "copy-paste.txt"]
    |> Enum.each(fn f ->
      case File.lstat(f) do
        {:ok, %File.Stat{type: :symlink}} ->
          File.rm!(f)

          [file | _] =
            Enum.filter(past_files, fn x -> String.ends_with?(x, f) end)
            |> Enum.sort()
            |> Enum.reverse()

          File.ln_s!("past/" <> file, f)

        {:ok, %File.Stat{type: _}} ->
          IO.puts("File #{f} is not a symlink, skipping ...")

        _ ->
          IO.puts("File #{f} does not exist!")
          exit(-1)
      end
    end)
  end
end
