defmodule Temple.Generator do
  @template ~s(<p style="text-align: center;"><strong>Sunday Worship</strong></p>
<p style="text-align: center;"><strong><%= date %></strong></p>
&nbsp;
<p style="text-align: center;"><strong>“<%= title %>”</strong></p>
<p style="text-align: center;"><%= passage %></p>
<p style="text-align: center;"><%= preacher %></p>
&nbsp;

<a href="<%= url %>">Sermon Audio</a>

&nbsp;

<strong>Sermon Outline:</strong>
<ol>
  <%= for point <- points do %>
 	<li><%= point %></li>
  <% end %>
</ol>
<strong>Reflection Questions:</strong>
<ol>
  <%= for q <- questions do %>
 	<li><%= q %></li>
  <% end %>
</ol>)

  #
  #   @quote_template ~s(<p style="text-align: center;"><strong>Sunday Worship</strong></p>
  # <p style="text-align: center;"><strong><%= date %></strong></p>
  # &nbsp;
  # <p style="text-align: center;"><strong>“<%= title %>”</strong></p>
  # <p style="text-align: center;"><%= passage %></p>
  # <p style="text-align: center;"><%= preacher %></p>
  # &nbsp;
  #
  # <a href="<%= url %>">Sermon Audio</a>
  #
  # &nbsp;
  #
  # <strong>Sermon Outline:</strong>
  # <ol>
  #   <%= for point <- points do %>
  #  	<li><%= point %></li>
  #   <% end %>
  # </ol>
  #
  # <strong>Meditation Quotes:</strong>
  # <%= for q <- quotes do %>
  #     <blockquote>
  #       quote.text
  #       <cite>q.author</cite>
  #     </blockquote>
  # <% end %>
  #
  # <strong>Reflection Questions:</strong>
  # <ol>
  #   <%= for q <- questions do %>
  #  	<li><%= q %></li>
  #   <% end %>
  # </ol>)

  def generate(
        %Temple.Model{
          title: title,
          passage: passage,
          preacher: preacher,
          points: points,
          questions: questions
        },
        url
      ) do
    sun = Date.utc_today() |> Date.beginning_of_week(:sunday)

    EEx.eval_string(@template,
      title: title,
      passage: passage,
      preacher: preacher,
      points: points,
      questions: questions,
      date:
        sun
        |> Calendar.strftime("%B " <> Ordinal.ordinalize(sun.day) <> ", %Y"),
      url: url |> String.replace("&", "&amp;") |> String.replace("www", "dl")
    )
  end
end
