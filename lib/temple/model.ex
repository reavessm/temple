defmodule Temple.Model do
  defstruct [:preacher, :title, :passage, :points, :questions]

  def build({:ok, content, _rest, _context, _position, _byte_offest}) do
    model_mapping =
      content
      |> Enum.reduce(
        %{preacher: "", title: "", passage: "", points: [], questions: []},
        fn i, acc ->
          case i do
            {:preacher, [preacher | []]} ->
              %{
                preacher: preacher,
                title: acc[:title],
                passage: acc[:passage],
                points: acc[:points],
                questions: acc[:questions]
              }

            {:title, [title | []]} ->
              %{
                preacher: acc[:preacher],
                title: title,
                passage: acc[:passage],
                points: acc[:points],
                questions: acc[:questions]
              }

            {:passage, [passage | []]} ->
              %{
                preacher: acc[:preacher],
                title: acc[:title],
                passage: passage,
                points: acc[:points],
                questions: acc[:questions]
              }

            {:point, [point | []]} ->
              %{
                preacher: acc[:preacher],
                title: acc[:title],
                passage: acc[:passage],
                points: acc[:points] ++ [point],
                questions: acc[:questions]
              }

            {:question, [question | []]} ->
              %{
                preacher: acc[:preacher],
                title: acc[:title],
                passage: acc[:passage],
                points: acc[:points],
                questions: acc[:questions] ++ [question]
              }

            _ ->
              acc
          end
        end
      )

    %Temple.Model{
      preacher: model_mapping[:preacher],
      title: model_mapping[:title],
      passage: model_mapping[:passage],
      points: model_mapping[:points],
      questions: model_mapping[:questions]
    }
  end
end
