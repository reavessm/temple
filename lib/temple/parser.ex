defmodule Temple.Parser do
  import NimbleParsec

  words =
    utf8_string(
      [
        ?",
        ?&,
        ?',
        ?(,
        ?),
        ?,,
        # These are all different hypens
        ?-,
        ?—,
        ?–,
        ?_,
        ?.,
        ?/,
        ?0..?9,
        ?:,
        ?;,
        ?!,
        ??,
        ?A..?Z,
        ?\s,
        ?a..?z,
        ?‘,
        ?‘,
        ?’,
        ?“,
        ?[,
        ?],
        ?”
      ],
      min: 1
    )

  #
  # quote_char =
  #   utf8_string(
  #     [
  #       ?",
  #       ?‘,
  #       ?‘,
  #       ?’,
  #       ?“,
  #       ?”
  #     ],
  #     min: 1
  #   )

  preacher =
    optional(ignore(string("\n")))
    |> ignore(string("Preaching of the Word "))
    |> concat(words)
    |> ignore(string("\n"))
    |> tag(:preacher)

  title =
    optional(ignore(string("\n")))
    |> concat(words)
    |> optional(ignore(string("\n")))
    |> tag(:title)

  passage =
    optional(ignore(string("\n")))
    |> repeat(words)
    |> optional(ignore(string("\n")))
    |> tag(:passage)

  point =
    optional(repeat(ignore(string("\n"))))
    |> ignore(repeat(string("I")))
    |> ignore(repeat(string("V")))
    |> ignore(string(". "))
    |> concat(words)
    |> optional(ignore(string("\n")))
    |> tag(:point)

  # quote =
  #   optional(repeat(ignore(string("\n"))))
  #   |> ignore(repeat(string("- Meditation Quotes -")))
  #   |> optional(repeat(ignore(string("\n"))))
  #   |> ignore(concat(quote_char))
  #     |> concat(words)

  page =
    optional(repeat(ignore(string("\n"))))
    |> optional(ignore(string("- 4 -")))
    |> optional(repeat(ignore(string(" "))))
    |> optional(ignore(string("- 5 -")))
    |> optional(repeat(ignore(string("\n"))))
    |> optional(repeat(choice([
      ignore(string("-")),
      ignore(string(" ")),
    ])))
    |> optional(ignore(string("Reflection Questions")))
    |> optional(repeat(choice([
      ignore(string("-")),
      ignore(string(" ")),
    ])))

  question =
    optional(repeat(ignore(string("\n"))))
    |> ignore(integer(1))
    |> choice([
      ignore(string(") ")),
      ignore(string(". ")),
    ])
    |> concat(words)
    |> optional(repeat(ignore(string("\n"))))
    |> tag(:question)

  defparsec(
    :parse,
    preacher
    |> concat(title)
    |> concat(passage)
    |> repeat(point)
    |> ignore(page)
    |> repeat(question)
  )
end
